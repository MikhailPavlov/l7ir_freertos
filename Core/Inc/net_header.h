/*
 * net_header.h
 *
 *  Created on: Feb 1, 2021
 *      Author: admin
 */

#ifndef SRC_USER_LIBS_NET_HEADER_H_
#define SRC_USER_LIBS_NET_HEADER_H_
#include "user.h"
   #ifdef __cplusplus
#include "Buffer.h"
   #endif
//#include "all_devices_include.h"
extern uint32_t mask_buf[];
typedef struct
{
	uint8_t stack_number;
	uint8_t net_ID;
	uint8_t command;
	uint8_t error_count;
	void (*call_back)();

	void (*send_call_back)();
	uint32_t net_time_out;
	uint32_t net_count;

} net_abonent;

//net_abonent net_buffer[100];
typedef enum
{
 ITS_BUSY =0,
 ITS_FREE=1
}BUSY_FLAG;

typedef struct
{
	net_abonent net_stask[255];
	uint8_t stack_size;
	BUSY_FLAG net_busy;
}all_net_abonents;

extern all_net_abonents net_abonents;
void all_devices_ini();

#endif /* SRC_USER_LIBS_NET_HEADER_H_ */
