/*
 * Buffer.h
 *
 *  Created on: Jan 23, 2021
 *      Author: admin
 */

#ifndef SRC_USER_LIBS_BUFFER_H_
#define SRC_USER_LIBS_BUFFER_H_

#include "user.h"

//======NET CODE=========================
#define SOH   	0x24//0xFF
#define DLE   	0x2F//0x10
#define ETX   	0x3B//0x03
//======end NET CODE=====================

class cycle_buffer
{
private:
	uint8_t *buf;
	uint8_t index_rd;
	uint8_t index_wr;
	uint8_t size;
	bool is_empty;
	bool is_full;

public:
 cycle_buffer(uint8_t *buffer);
 bool push(uint8_t data);
 uint8_t pop();
 bool get_empty(){return is_empty;};
 bool get_full(){return is_full;};
 uint8_t get_fill();
 bool insert_crc();
 bool check_crc();
 uint8_t dow_crc();
 bool insert_stsp();
 bool del_stsp();
 void clean();
};



#endif /* SRC_USER_LIBS_BUFFER_H_ */
