/*
 * all_devices_include.h
 *
 *  Created on: Feb 1, 2021
 *      Author: admin
 */

#ifndef SRC_DEVICES_ALL_DEVICES_INCLUDE_H_
#define SRC_DEVICES_ALL_DEVICES_INCLUDE_H_

uint8_t no_func(uint8_t net_count, uint8_t temp);// {return net_count;}
#define DEVICE_QUANT 0
#define MAX_DEV_LIMIT 5
#include "encoder1.h"
#define DEV1_FUNC encoder1_ini

#ifndef DEV1_FUNC
#define DEV1_FUNC no_func
#endif
#ifndef DEV2_FUNC
#define DEV2_FUNC no_func
#endif
#ifndef DEV3_FUNC
#define DEV3_FUNC no_func
#endif
#ifndef DEV4_FUNC
#define DEV4_FUNC no_func
#endif
#ifndef DEV5_FUNC
#define DEV5_FUNC no_func
#endif
#ifndef DEV6_FUNC
#define DEV6_FUNC no_func
#endif
#ifndef DEV7_FUNC
#define DEV7_FUNC no_func
#endif
#ifndef DEV8_FUNC
#define DEV8_FUNC no_func
#endif
#ifndef DEV9_FUNC
#define DEV9_FUNC no_func
#endif
#ifndef DEV10_FUNC
#define DEV10_FUNC no_func
#endif
#ifndef DEV11_FUNC
#define DEV11_FUNC no_func
#endif
#ifndef DEV12_FUNC
#define DEV12_FUNC no_func
#endif
#ifndef DEV13_FUNC
#define DEV13_FUNC no_func
#endif
#ifndef DEV14_FUNC
#define DEV14_FUNC no_func
#endif
#ifndef DEV15_FUNC
#define DEV15_FUNC no_func
#endif
#ifndef DEV16_FUNC
#define DEV16_FUNC no_func
#endif
#ifndef DEV17_FUNC
#define DEV17_FUNC no_func
#endif
#ifndef DEV18_FUNC
#define DEV18_FUNC no_func
#endif
#ifndef DEV19_FUNC
#define DEV19_FUNC no_func
#endif
#ifndef DEV20_FUNC
#define DEV20_FUNC no_func
#endif
#ifndef DEV21_FUNC
#define DEV21_FUNC no_func
#endif
#ifndef DEV22_FUNC
#define DEV22_FUNC no_func
#endif
#ifndef DEV23_FUNC
#define DEV23_FUNC no_func
#endif
#ifndef DEV24_FUNC
#define DEV24_FUNC no_func
#endif



extern uint8_t (*ini_func[5])(uint8_t, uint8_t);// = { DEV1_FUNC, DEV2_FUNC, DEV3_FUNC, DEV4_FUNC, DEV5_FUNC};



//void* (ini_func++) = encoder1_ini;


#endif /* SRC_DEVICES_ALL_DEVICES_INCLUDE_H_ */
