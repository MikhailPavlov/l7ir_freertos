/*
 * encoder1.cpp
 *
 *  Created on: Feb 1, 2021
 *      Author: admin
 */
#include <net_header.h>
#include "encoder1.h"
#include "Buffer.h"

net_abonent encoder1;//Обязательный элемент, должен заполняться для каждого сетевого события (если на устройство идет нескольк команд - нужно несколько таких элементов
#define ID_ENCODER1_1  0x32
#define ID_ENCODER1_2  0x30
//extern net_abonent encoder1;
void encoder1_protocol();
void make_send_buffer();
//uint8_t encoder1_ini(uint8_t, uint8_t);
net_abonent get_event_ini(uint8_t, uint8_t);
extern cycle_buffer tx_buf, rx_buf;
#define TO_LEFT 0x42
#define TO_RIGHT 0x43
#define KEY_CLICK 0x01
#define KEY_ENTER 0x07

/*######################################################
 * Обязательная функция, ссылку на нее нужно вписать
 * в файле  all_devices_include.h
 *
 * #####################################################*/
uint8_t encoder1_ini(uint8_t net_id, uint8_t net_count)
{
	net_abonents.net_stask[net_count] = get_event_ini(net_id,net_count); //инициализация сетевого события и помещение его в буфер
 net_count++;
 return net_count;
}


net_abonent get_event_ini(uint8_t net_id, uint8_t net_count) //зачем я делал дополнительный сетевой ID?
{
	 encoder1.net_ID = ID_ENCODER1_1;
    // encoder1.net_ID = net_id;
     encoder1.command = 0x01;
     encoder1.net_time_out = 100;
     encoder1.call_back = encoder1_protocol;
     encoder1.send_call_back = make_send_buffer;
     encoder1.stack_number = net_count;
     encoder1.error_count = 0;
     //encoder1.tx_buffer = &tx_buf;
     return encoder1;
}




void encoder1_protocol()
{
  rx_buf.del_stsp();
  if(!rx_buf.check_crc()) {encoder1.error_count++; return;}
  encoder1.error_count = 0;
  if(rx_buf.pop()!=encoder1.net_ID){encoder1.error_count++; return;}
  if(rx_buf.pop()!=encoder1.command){encoder1.error_count++; return;}
  uint8_t count = rx_buf.pop();
  uint8_t data;
  for(uint8_t i=0; i<count;i++)
  {
    data = rx_buf.pop();
    switch (data)
    {
     case TO_LEFT:
    	 break;
     case TO_RIGHT:
    	 break;
     case KEY_CLICK:
    	 break;
     case KEY_ENTER:
    	 break;
    }
  }
}

void make_send_buffer()
{
	tx_buf.push(encoder1.net_ID);
	tx_buf.push(encoder1.command);
	tx_buf.insert_crc();
	tx_buf.insert_stsp();
}
