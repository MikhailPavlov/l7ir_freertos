/*
 * net_user_tasks.cpp
 *
 *  Created on: Feb 1, 2021
 *      Author: admin
 */
#include "net_header.h"
#include "all_devices_include.h"
#include "Buffer.h"

all_net_abonents net_abonents;

uint32_t mask_buf[32] ={0};
uint8_t no_func(uint8_t net_count, uint8_t temp) {return net_count;}
extern uint8_t transmitBuffer[32];
extern uint8_t receiveBuffer[32];

uint8_t (*ini_func[MAX_DEV_LIMIT])(uint8_t, uint8_t) = { DEV1_FUNC, DEV2_FUNC, DEV3_FUNC, DEV4_FUNC, DEV5_FUNC};
cycle_buffer tx_buf(transmitBuffer), rx_buf(receiveBuffer);

void all_devices_ini()
{
	uint8_t curr_number;
	net_abonents.stack_size =0;
	net_abonents.net_busy = ITS_FREE;
 for(uint8_t i=0;i <MAX_DEV_LIMIT;i++)
 {
   curr_number = net_abonents.stack_size;
   net_abonents.stack_size = (*ini_func[i])(i,curr_number);

 }
}
