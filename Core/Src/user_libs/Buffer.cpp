/*
 * Buffer.cpp
 *
 *  Created on: Jan 23, 2021
 *      Author: admin
 */

#include "Buffer.h"


 cycle_buffer::cycle_buffer(uint8_t *pointer)
{
 buf= pointer;
 size = sizeof(*pointer);
 index_rd = 0;
 index_wr =0;
 is_full = false;
 is_empty = true;
}

 bool cycle_buffer::push(uint8_t data)
 {
	 if(!is_full)
	 {
		 *(buf+index_wr)=data;
		 index_wr++;
		 if(index_wr==size) index_wr =0;
		 if(index_wr==index_rd) is_full = true;
	 return true;

	 }
	 return false;
 }

 uint8_t cycle_buffer::pop()
 {
	 uint8_t data;
	 if(!is_empty)
	 {
		 data = *(buf+index_rd);
		 index_rd++;
		 if(index_rd==size)index_rd=0;
		 if(index_rd==index_wr)is_empty=true;

	 }
	 return data;
 }

 uint8_t cycle_buffer::get_fill()
 {

	 if(is_empty) return 0;
	 if(is_full) return size;
	 if(index_wr>index_rd) return (index_wr-index_rd);
	 return (size-index_rd+index_wr);
 }

 bool cycle_buffer::insert_stsp()
 {
  uint8_t temp, len,i;
  len = get_fill();
  if(!push(SOH)) return false;
  for(i=0;i<len;i++)
	  {
	   temp = pop();
	   if((temp==SOH)||(temp==DLE)||(temp==ETX))
	   {
		   if(!push(DLE)) return false;
		   temp=~temp;
	   }
	   if(!push(temp)) return false;
      }
  if(!push(ETX)) return false;
  return true;

 }

 bool cycle_buffer::del_stsp()
 {
	  uint8_t temp, len,i;
	  len = get_fill();
	  for(i=0;i<len;i++)
	  {
		  temp = pop();
		  if((temp!=SOH)&&(temp!=ETX))
		  {
			  if(temp==DLE)
			  {
				  temp = pop();
				  temp=~temp;
				  len--;
			  }
			  if(!push(temp)) return false;

		  }
	  }
	  return true;
 }

 uint8_t cycle_buffer::dow_crc()
 {
	 uint8_t temp, count, fl,i,j,crc=0;
	 count = get_fill();
	 for(j=0;j<count;j++)
	 {
		temp = pop();
		push(temp);
		crc = crc^temp;
		for(i=0;i<8;i++)
		{
			fl=crc&1;
			if(fl)
			{
				crc = crc^18;
				crc = 0x80|(crc>>1);
			}
			else crc = (crc>>1);
		}
	 }
	 return crc;
 }

 bool cycle_buffer::check_crc()
 {
	 uint8_t chcrc;
	 chcrc = dow_crc();
	 if(chcrc==0)
	 {
		 if(index_wr == 0)index_wr = size-1;
		 else index_wr--;
		 return true;
	 }
	 else
	 {
		 clean();
		 return false;
	 }
 }

 bool cycle_buffer::insert_crc()
 {
	 uint8_t count, outcrc;
	 count = get_fill();
	 if(2*count<=size)
	 {
		 outcrc = dow_crc();
		 if(!push(outcrc)) return false;
		 return true;
	 }
	 return false;
 }

 void cycle_buffer::clean()
 {
	 index_rd=index_wr;
	 is_empty = true;
	 is_full = false;
 }
